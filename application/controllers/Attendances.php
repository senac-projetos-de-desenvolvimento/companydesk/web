<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Attendances extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }
        $this->load->model('attendances_model');
    }

    public function index(){
        $data = array(
            'title' => 'Atendimentos cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',              
            ),
            //'attendances' => $this->core_model->get_all('attendances'),
            'attendances' => $this->attendances_model->get_all(),
        );

        /*
        echo '<pre>';
        print_r($data['attendances']);
        exit();
        */

        $this->load->view('layout/header',$data);
        $this->load->view('attendances/index');
        $this->load->view('layout/footer');
    }

    public function edit($attendance_id = NULL){
        if(!$attendance_id || !$this->core_model->get_by_id('attendances', array('attendances_id' => $attendance_id))){
            $this->session->set_flashdata('error','Atendimento não encontrado.');
            redirect('attendances');
        } else {
            $this->form_validation->set_rules('attendances_activities_id','','trim|required');
            $this->form_validation->set_rules('attendances_customers_end_id','','trim|required');
            $this->form_validation->set_rules('attendances_customers_partner_id','','trim|required');
            $this->form_validation->set_rules('attendances_users_id','','trim|required');        
            $this->form_validation->set_rules('attendances_origins_id','','trim|required');
            $this->form_validation->set_rules('attendances_description','','trim|required|max_length[500]');
            if($this->form_validation->run()){
                $data = elements(
                    array(   
                        'attendances_activities_id',      
                        'attendances_customers_end_id',
                        'attendances_customers_partner_id',
                        'attendances_users_id',
                        'attendances_origins_id',
                        'attendances_description',                        
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('attendances', $data, array('attendances_id'=>$attendance_id));
                redirect('attendances');
            }else{

                $data = array(
                    'title' => 'Atualizar atendimento',
                    'styles' => array(
                        'vendor/jquery/jquery-ui.min.css', 
                    ),
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                        'vendor/jquery/jquery-ui.min.js', 
                        'vendor/js/autocomplete.js',
                    ),
                    'attendance' => $this->core_model->get_by_id('attendances', array('attendances_id' => $attendance_id)),
                    'origins' => $this->core_model->get_all('origins'),
                    'users' => $this->core_model->get_all('users'),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('attendances/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){
        $this->form_validation->set_rules('attendances_description','','trim|required|max_length[500]');
        $this->form_validation->set_rules('attendances_start','','trim|required');
        $this->form_validation->set_rules('attendances_end','','trim|required');
        $this->form_validation->set_rules('attendances_customers_partner_id','','trim|required');
        $this->form_validation->set_rules('attendances_customers_end_id','','trim|required');
        $this->form_validation->set_rules('attendances_activities_id','','trim|required');
        $this->form_validation->set_rules('attendances_origins_id','','trim|required');
        // $this->form_validation->set_rules('attendances_users_id','','trim|required');        
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'attendances_description',
                    'attendances_start',
                    'attendances_end',
                    'attendances_customers_partner_id',
                    'attendances_customers_end_id',
                    'attendances_activities_id',
                    'attendances_origins_id',
                ),
                $this->input->post()                
            );
            $data['attendances_users_id'] = $this->ion_auth->get_user_id();
            if($data['attendances_end'] == ""){
                $data['attendances_end'] = NULL;                   
            }  
            $data = html_escape($data);            
            if($this->core_model->insert('attendances', $data)){
                $this->session->set_flashdata('success', 'Dados salvos com sucesso.');
            }else{
                $this->session->set_flashdata('error', 'Erro ao salvar dados.');
            }
            redirect('attendances');
        }else{
            $data = array(
                'title' => 'Cadastrar atendimento',
                'styles' => array(
                        'vendor/jquery/jquery-ui.min.css', 
                    ),
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                    'vendor/jquery/jquery-ui.min.js', 
                    'vendor/js/autocomplete.js',
                ),   
                'origins' => $this->core_model->get_all('origins'),
                'users' => $this->core_model->get_all('users'),
            );
            $this->load->view('layout/header',$data);
            $this->load->view('attendances/add');
            $this->load->view('layout/footer');           
        }        
    }

    public function showDescription($attendance_id){                   
        $data = array(
            'title' => 'Mostrar Atendimento',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'attendance' => $this->core_model->get_by_id('attendances', array('attendances_id' => $attendance_id)),
            'customers' => $this->core_model->get_all('customers'),
            'activities' => $this->core_model->get_all('activities'),
            'origins' => $this->core_model->get_all('origins'),
            'users' => $this->core_model->get_all('users'),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('attendances/showDescription');
        $this->load->view('layout/footer');
    }

    public function showActivity($activity_id){
        $data = array(
            'title' => 'Mostrar Atividade',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'activity' => $this->core_model->get_by_id('activities', array('activities_id' => $activity_id)),
            'customers' => $this->core_model->get_all('customers'),
            'users' => $this->core_model->get_all('users'),
            'origins' => $this->core_model->get_all('origins'),
            'priorities' => $this->core_model->get_all('priorities'),
            'automations' => $this->core_model->get_all('automations'),
            'types_identifier' => $this->core_model->get_all('types_identifier'),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('attendances/showActivity');
        $this->load->view('layout/footer');
    }

    public function showCalled($called_id){
        $data = array(
            'title' => 'Mostrar Chamado',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'called' => $this->core_model->get_by_id('calleds', array('calleds_id' => $called_id)),
            'status' => $this->core_model->get_all('status'),
            'users' => $this->core_model->get_all('users'),
            'call_types' => $this->core_model->get_all('call_types'),
            'priorities' => $this->core_model->get_all('priorities'),
            'customers' => $this->core_model->get_all('customers'),
            'automations' => $this->core_model->get_all('automations'),
            'types_identifier' => $this->core_model->get_all('types_identifier'),            
        );
        $this->load->view('layout/header', $data);
        $this->load->view('attendances/showCalled');
        $this->load->view('layout/footer');
    }
    public function showCustomer($customer_id){
        $data = array(
            'title' => 'Mostrar Cliente',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'customer' => $this->core_model->get_by_id('customers', array('customers_id' => $customer_id)),
            'automations' => $this->core_model->get_all('automations'),
            'types_identifier' => $this->core_model->get_all('types_identifier'), 
            'pumps_brand' => $this->core_model->get_all('pumps_brand'),
            'types_company' => $this->core_model->get_all('types_company'),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('calleds/showCustomer');
        $this->load->view('layout/footer');
    }

    public function searchCustomersEnd(){        
        $rows = $this->attendances_model->getCustomersEndAutoComplete($_GET['term']);
        if(count($rows) > 0){
            foreach ($rows as $row) {
                $dados[] = array('label'		 =>		$row['customers_social_reason'],
                                    'id'	 	 =>		$row['customers_id'],
                                    'value'		 =>		$row['customers_social_reason']	);
            }
            echo json_encode($dados);
        }
    }

    public function searchCustomersPartner(){
        $rows = $this->attendances_model->getCustomersEndAutoComplete($_GET['term']);
        if(count($rows) > 0){
            foreach ($rows as $row) {
                $dados[] = array('label'		 =>		$row['customers_social_reason'],
                                    'id'	 	 =>		$row['customers_id'],
                                    'value'		 =>		$row['customers_social_reason']	);
            }
            echo json_encode($dados);
        }
    }

    public function addWithActivity($activity_id = NULL){
        $this->form_validation->set_rules('attendances_description','','trim|required|max_length[500]');
        $this->form_validation->set_rules('attendances_start','','trim|required');
        $this->form_validation->set_rules('attendances_end','','trim|required');
        $this->form_validation->set_rules('attendances_customers_partner_id','','trim|required');
        $this->form_validation->set_rules('attendances_customers_end_id','','trim|required');
        // $this->form_validation->set_rules('attendances_activities_id','','trim|required');
        $this->form_validation->set_rules('attendances_origins_id','','trim|required');
        // $this->form_validation->set_rules('attendances_users_id','','trim|required');        
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'attendances_description',
                    'attendances_start',
                    'attendances_end',
                    'attendances_customers_partner_id',
                    'attendances_customers_end_id',
                    'attendances_origins_id',
                ),
                $this->input->post()                
            );
            $data['attendances_activities_id'] = $activity_id;
            $data['attendances_users_id'] = $this->ion_auth->get_user_id();

            if($data['attendances_end'] == ""){
                $data['attendances_end'] = NULL;                   
            }  

            // echo '<pre>';
            // print_r($data);
            // exit();

            $data = html_escape($data);            
            if($this->core_model->insert('attendances', $data)){
                $this->session->set_flashdata('success', 'Dados salvos com sucesso.');
            }else{
                $this->session->set_flashdata('error', 'Erro ao salvar dados.');
            }
            redirect('attendances');
        }else{
            $data = array(
                'title' => 'Cadastrar atendimento',
                'styles' => array(
                        'vendor/jquery/jquery-ui.min.css', 
                    ),
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                    'vendor/jquery/jquery-ui.min.js', 
                    'vendor/js/autocomplete.js',
                ),   
                'activity_id' => $activity_id,
                'origins' => $this->core_model->get_all('origins'),
                'users' => $this->core_model->get_all('users'),
            );
            $this->load->view('layout/header',$data);
            $this->load->view('attendances/addWithActivity');
            $this->load->view('layout/footer');           
        }
    }
}