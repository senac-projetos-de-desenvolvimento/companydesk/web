<?php
defined('BASEPATH') OR exit ('Ação não autorizada.');  //Para não permitir usuário acessar direto o controller

class Home extends CI_Controller{
        public function __construct(){
            parent::__construct();
            if(!$this->ion_auth->logged_in()){
                $this->session->set_flashdata('info','Seu sessão expirou.');
                redirect('login');
            }
			$this->load->model('home_model');
        }
        
        public function index(){        
            $user = $this->ion_auth->user()->row();    
            $data = array(
                'title' => 'Bem vindo ao CompanyDesk',
                
                'styles' => array(
                    'vendor/datatables/dataTables.bootstrap4.min.css',            
                ),
                'scripts' => array(
                    'vendor/datatables/jquery.dataTables.min.js',
                    'vendor/datatables/dataTables.bootstrap4.min.js',
                    'vendor/datatables/app.js',
                ),       
                'attendancesLast30Days' => $this->home_model->attendancesLast30Days($user->id),  
                'activitiesLast30Days' => $this->home_model->activitiesLast30Days($user->id), 
                'calledsLast30Days' => $this->home_model->calledsLast30Days($user->id),  
                'calledsIndexOpenToday' => $this->home_model->calledsOpenToday($user->id),  
                'calledsIndexCloseToday' => $this->home_model->calledsCloseToday($user->id),  
                'lastThreeCalleds' => $this->home_model->lastThreeCalleds(),
                'attendancesIndexToday' => $this->home_model->attendancesToday(),
                'activitiesIndexToday' => $this->home_model->activitiesToday(),
                'calledsIndexToday' => $this->home_model->calledsToday(),
            );

            $count = 0;

            $actLow = $this->home_model->activitiesLowPriorityWinning();
            $actMedium = $this->home_model->activitiesMediumPriorityWinning();
            $actHigh = $this->home_model->activitiesHighPriorityWinning();

            //  var_dump($actLow->numberActivitiesLow);
            //  var_dump($actMedium->numberActivitiesMedium);
            //  var_dump($actHigh->numberActivitiesHigh);

            if($actLow->numberActivitiesLow != 0){
                $data['issetActivitiesWinningLow'] = TRUE;
                $data['activitiesLowWinning'] = $this->home_model->activitiesLowPriorityWinning();
                $count ++;
            }else{
                $data['issetActivitiesWinningLow'] = FALSE;
            }

            if($actMedium->numberActivitiesMedium != 0){
                $data['issetActivitiesWinningMedium'] = TRUE;
                $data['activitiesMediumWinning'] = $this->home_model->activitiesMediumPriorityWinning();
                $count ++;
            }else{
                $data['issetActivitiesWinningMedium'] = FALSE;
            }

            if($actHigh->numberActivitiesHigh != 0){
                $data['issetActivitiesWinningHigh'] = TRUE;
                $data['activitiesHighWinning'] = $this->home_model->activitiesHighPriorityWinning();
                $count ++;
            }else{
                $data['issetActivitiesWinningHigh'] = FALSE;
            }

            $data['counter'] = $count;

            $this->load->view('layout/header', $data); //passando parâmetro data para a view
            $this->load->view('home/index');
            $this->load->view('layout/footer');
        }
    }