<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Priorities extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
       // $this->load->model('activities_model');
    }

    public function index(){

        $data = array(
            'title' => 'Prioridades cadastradas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'priorities' => $this->core_model->get_all('priorities'),
        );

        /*
        echo '<pre>';
        print_r($data['priorities']);
        exit();
        */
        
        $this->load->view('layout/header',$data);
        $this->load->view('priorities/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($priority_id = NULL){
        if(!$priority_id || !$this->core_model->get_by_id('priorities', array('priorities_id' => $priority_id))){
            $this->session->set_flashdata('error','Tipo de prioridade não encontrada.');
            redirect('priorities');
        } else {
            $this->form_validation->set_rules('priorities_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'priorities_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('priorities', $data, array('priorities_id'=>$priority_id));

                redirect('priorities');
            }else{

                $data = array(
                    'title' => ' Atualizar tipo de prioridade',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'priority' => $this->core_model->get_by_id('priorities', array('priorities_id' => $priority_id)),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('priorities/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('priorities_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'priorities_description',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('priorities', $data);
            redirect('priorities');
        }else{
            $data = array(
                'title' => 'Cadastrar tipo de prioridade',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('priorities/add');
            $this->load->view('layout/footer');           
        }
    }
    public function delete($priority_id = NULL){
        if(!$priority_id || !$this->core_model->get_by_id('priorities', array('priorities_id' => $priority_id))){
            $this->session->set_flashdata('error','Tipo de prioridade não encontrada.');
            redirect('priorities');
        } else {
            $this->core_model->delete('priorities', array('priorities_id'=>$priority_id));
            redirect('priorities');
        }
    }
}