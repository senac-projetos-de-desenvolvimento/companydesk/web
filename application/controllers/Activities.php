<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Activities extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
        $this->load->model('attendances_model');
        $this->load->model('activities_model');
    }

    public function index(){
        $data = array(
            'title' => 'Atividades cadastradas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            //'activities' => $this->core_model->get_all('activities'),
            'activities' => $this->activities_model->get_all(),
        );

        //echo '<pre>';
        //print_r($data['activities']);
        //exit();
        

        $this->load->view('layout/header',$data);
        $this->load->view('activities/index');
        $this->load->view('layout/footer');
    }

    public function edit($activity_id = NULL){
        if(!$activity_id || !$this->core_model->get_by_id('activities', array('activities_id' => $activity_id))){
            $this->session->set_flashdata('error','Atividade não encontrada.');
            redirect('activities');
        } else {
            $this->form_validation->set_rules('activities_description','','trim|required|max_length[500]');
            $this->form_validation->set_rules('activities_users_id','','trim|required'); 
            $this->form_validation->set_rules('activities_calleds_id','','trim|required');
            $this->form_validation->set_rules('activities_customers_partner_id','','trim|required');
            $this->form_validation->set_rules('activities_customers_end_id','','trim|required');
            $this->form_validation->set_rules('activities_priorities_id','','trim|required');
            $this->form_validation->set_rules('activities_call_types_id','','trim|required');
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'activities_description',
                        'activities_users_id',
                        'activities_calleds_id',
                        'activities_customers_partner_id',
                        'activities_customers_end_id',
                        'activities_priorities_id',    
                        'activities_call_types_id',                    
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('activities', $data, array('activities_id'=>$activity_id));

                redirect('activities');
            }else{

                $data = array(
                    'title' => ' Atualizar atividade',
                    'styles' => array(
                        'vendor/jquery/jquery-ui.min.css', 
                    ),
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                        'vendor/jquery/jquery-ui.min.js', 
                        'vendor/js/autocomplete.js',
                    ),
                    'activity' => $this->core_model->get_by_id('activities', array('activities_id' => $activity_id)),
                    'call_types' => $this->core_model->get_all('call_types'),
                    'priorities' => $this->core_model->get_all('priorities'),
                    'automations' => $this->core_model->get_all('automations'),
                    'types_identifier' => $this->core_model->get_all('types_identifier'),
                    'users' => $this->core_model->get_all('users'),
                    'status' => $this->core_model->get_all('status'),
                    'calleds' => $this->core_model->get_all('calleds'),
                    'customers' => $this->core_model->get_all('customers'),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('activities/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){
        $this->form_validation->set_rules('activities_start','','trim|required');
        $this->form_validation->set_rules('activities_users_id','','trim|required'); 
        $this->form_validation->set_rules('activities_customers_partner_id','','trim|required');
        $this->form_validation->set_rules('activities_customers_end_id','','trim|required');
        $this->form_validation->set_rules('activities_origins_id','','trim|required');
        $this->form_validation->set_rules('activities_calleds_id','','trim|required');
        $this->form_validation->set_rules('activities_priorities_id','','trim|required');
        $this->form_validation->set_rules('activities_automations_id','','trim|required');
        $this->form_validation->set_rules('activities_types_identifier_id','','trim|required');
        $this->form_validation->set_rules('activities_status_id','','trim|required');
        $this->form_validation->set_rules('activities_call_types_id','','trim|required');

        // $config["upload_path"] = "http://localhost/companydesk/public/img/imagesEmail/";//$this->uploadPath;
        //     $config["max_size"] = 2048;
        //     $config["allowed_types"] = "gif|jpg|jpeg|png";
    
        //     $this->load->library('upload',$config);
        //     if(!$this->upload->do_upload('activities_atachments')){
        //         $msg = $this->upload->display_errors();
        //         $this->session->set_flashdata('error',$msg);
        //     }else{
        //         $msg = 'Upload realizado com sucesso!';
        //         $this->session->set_flashdata('success',$msg);
        //     }

        if($this->form_validation->run()){
            
            $data = elements(
                array(   
                    'activities_description',
                    'activities_start',
                    'activities_end',
                    // 'activities_atachments',
                    'activities_users_id',
                    'activities_customers_partner_id',
                    'activities_customers_end_id',                    
                    'activities_origins_id',
                    'activities_calleds_id',
                    'activities_priorities_id',
                    'activities_automations_id',
                    'activities_types_identifier_id',
                    'activities_status_id',
                    'activities_call_types_id',
                ),
                $this->input->post()
            );
            if($data['activities_end'] == ""){
                $data['activities_end'] = NULL;                   
            }
            $data = html_escape($data);            
            if($this->core_model->insert('activities', $data)){
                $this->session->set_flashdata('success', 'Dados salvos com sucesso.');
            }else{
                $this->session->set_flashdata('error', 'Erro ao salvar dados.');
            }
            redirect('activities');
        }else{
            $data = array(
                'title' => 'Cadastrar atividades',
                'styles' => array(
                    'vendor/jquery/jquery-ui.min.css', 
                ),
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                    'vendor/jquery/jquery-ui.min.js', 
                    'vendor/js/autocomplete.js',
                ),  
                'users' => $this->core_model->get_all('users'),
                'call_types' => $this->core_model->get_all('call_types'),
                'origins' => $this->core_model->get_all('origins'),
                'priorities' => $this->core_model->get_all('priorities'),
                'automations' => $this->core_model->get_all('automations'),
                'types_identifier' => $this->core_model->get_all('types_identifier'),
                'customers' => $this->core_model->get_all('customers'),  
                'status' => $this->core_model->get_all('status'),            
            );
            $this->load->view('layout/header',$data);
            $this->load->view('activities/add');
            $this->load->view('layout/footer');           
        }
    }

    public function relatedAttendances($activity_id = NULL){
        $data = array(
            'title' => 'Atendimentos relacionados com a atividade id '.$activity_id,
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',              
            ),
            'attendances' => $this->activities_model->getAllAttendancesToActivity($activity_id),
        );
        //echo '<pre>';
        //print_r($data['endCustomers']);
        //exit();
        $this->load->view('layout/header',$data);
        $this->load->view('activities/showRelatedAttendances');
        $this->load->view('layout/footer');
    }

    public function showDescription($activity_id){                   
        $data = array(
            'title' => 'Mostrar Atividade',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'activity' => $this->core_model->get_by_id('activities', array('activities_id' => $activity_id)),
            'calleds' => $this->core_model->get_all('calleds'),
            'call_types' => $this->core_model->get_all('call_types'),
            'customers' => $this->core_model->get_all('customers'),
            'origins' => $this->core_model->get_all('origins'),
            'users' => $this->core_model->get_all('users'),
            'status' => $this->core_model->get_all('status'),
            'attendancesActivity' => $this->attendances_model->getAllAtendancesToActivity($activity_id),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('activities/showDescription');
        $this->load->view('layout/footer');
    }

    public function showCalled($called_id){
        $data = array(
            'title' => 'Mostrar Chamado',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'called' => $this->core_model->get_by_id('calleds', array('calleds_id' => $called_id)),
            'status' => $this->core_model->get_all('status'),
            'users' => $this->core_model->get_all('users'),
            'priorities' => $this->core_model->get_all('priorities'),
            'customers' => $this->core_model->get_all('customers'),
            'automations' => $this->core_model->get_all('automations'),
            'types_identifier' => $this->core_model->get_all('types_identifier'),            
        );
        $this->load->view('layout/header', $data);
        $this->load->view('activities/showCalled');
        $this->load->view('layout/footer');
    }

    public function showCustomer($customer_id){
        $data = array(
            'title' => 'Mostrar Cliente',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'customer' => $this->core_model->get_by_id('customers', array('customers_id' => $customer_id)),
            'automations' => $this->core_model->get_all('automations'),
            'types_identifier' => $this->core_model->get_all('types_identifier'), 
            'pumps_brand' => $this->core_model->get_all('pumps_brand'),
            'types_company' => $this->core_model->get_all('types_company'),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('calleds/showCustomer');
        $this->load->view('layout/footer');
    }

    public function searchCustomersEnd(){        
        $rows = $this->activities_model->getCustomersEndAutoComplete($_GET['term']);
        if(count($rows) > 0){
            foreach ($rows as $row) {
                $dados[] = array('label'		 =>		$row['customers_social_reason'],
                                    'id'	 	 =>		$row['customers_id'],
                                    'value'		 =>		$row['customers_social_reason']	);
            }
            echo json_encode($dados);
        }
    }

    public function searchCustomersPartner(){
        $rows = $this->activities_model->getCustomersEndAutoComplete($_GET['term']);
        if(count($rows) > 0){
            foreach ($rows as $row) {
                $dados[] = array('label'		 =>		$row['customers_social_reason'],
                                    'id'	 	 =>		$row['customers_id'],
                                    'value'		 =>		$row['customers_social_reason']	);
            }
            echo json_encode($dados);
        }
    }

    public function addWithCalled($called_id = NULL){
        $this->form_validation->set_rules('activities_start','','trim|required');
        // $this->form_validation->set_rules('activities_users_id','','trim|required'); 
        $this->form_validation->set_rules('activities_customers_partner_id','','trim|required');
        $this->form_validation->set_rules('activities_customers_end_id','','trim|required');
        $this->form_validation->set_rules('activities_origins_id','','trim|required');
        $this->form_validation->set_rules('activities_calleds_id','','trim|required');
        $this->form_validation->set_rules('activities_priorities_id','','trim|required');
        $this->form_validation->set_rules('activities_automations_id','','trim|required');
        $this->form_validation->set_rules('activities_types_identifier_id','','trim|required');
        $this->form_validation->set_rules('activities_status_id','','trim|required');
        $this->form_validation->set_rules('activities_call_types_id','','trim|required');

        if($this->form_validation->run()){
            
            $data = elements(
                array(   
                    'activities_description',
                    'activities_start',
                    'activities_end',
                    // 'activities_atachments',
                    // 'activities_users_id',
                    'activities_customers_partner_id',
                    'activities_customers_end_id',                    
                    'activities_origins_id',
                    // 'activities_calleds_id',
                    'activities_priorities_id',
                    'activities_automations_id',
                    'activities_types_identifier_id',
                    'activities_status_id',
                    'activities_call_types_id',
                ),
                $this->input->post()
            );
            $data['activities_calleds_id'] = $called_id;
            $data['activities_users_id'] = $this->ion_auth->get_user_id();
            if($data['activities_end'] == ""){
                $data['activities_end'] = NULL;                   
            }
            $data = html_escape($data);            
            if($this->core_model->insert('activities', $data)){
                $this->session->set_flashdata('success', 'Dados salvos com sucesso.');
            }else{
                $this->session->set_flashdata('error', 'Erro ao salvar dados.');
            }
            redirect('activities');
        }else{
            $data = array(
                'title' => 'Cadastrar atividades',
                'styles' => array(
                    'vendor/jquery/jquery-ui.min.css', 
                ),
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                    'vendor/jquery/jquery-ui.min.js', 
                    'vendor/js/autocomplete.js',
                ),  
                'called_id' => $called_id,
                'users' => $this->core_model->get_all('users'),
                'call_types' => $this->core_model->get_all('call_types'),
                'origins' => $this->core_model->get_all('origins'),
                'priorities' => $this->core_model->get_all('priorities'),
                'automations' => $this->core_model->get_all('automations'),
                'types_identifier' => $this->core_model->get_all('types_identifier'),
                'customers' => $this->core_model->get_all('customers'),  
                'status' => $this->core_model->get_all('status'),            
            );
            $this->load->view('layout/header',$data);
            $this->load->view('activities/addWithCalled');
            $this->load->view('layout/footer');           
        }
    }
}