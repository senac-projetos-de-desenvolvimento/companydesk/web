<?php
    defined('BASEPATH') OR exit('Ação não permitida');

class Customers extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }
        $this->load->model('customers_model');
        $this->load->model('calleds_model');
    }
    public function index(){
        $data = array(
            'title' => 'Clientes cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',              
            ),
            'customers' => $this->core_model->get_all('customers'),
        );
        //echo '<pre>';
        //print_r($data['endCustomers']);
        //exit();
        $this->load->view('layout/header',$data);
        $this->load->view('customers/index');
        $this->load->view('layout/footer');
    }

    public function edit($customer_id = NULL){
        if(!$customer_id || !$this->core_model->get_by_id('customers', array('customers_id' => $customer_id))){
            $this->session->set_flashdata('error', 'Cliente não cadastrado.');
            redirect('customers');
        } else {
            $this->form_validation->set_rules('customers_social_reason','','trim|required');
            $this->form_validation->set_rules('customers_fantasy_name','','trim|required');
            $this->form_validation->set_rules('customers_cnpj','','trim|required|exact_length[18]');
            $this->form_validation->set_rules('customers_state_registration','','trim|required');
            $this->form_validation->set_rules('customers_email','','trim|required|valid_email');
            $this->form_validation->set_rules('customers_telephone','','trim|required');
            $this->form_validation->set_rules('customers_address','','trim|required');
            $this->form_validation->set_rules('customers_address_number','','trim|required');
            $this->form_validation->set_rules('customers_neighborhood','','trim|required');
            $this->form_validation->set_rules('customers_city','','trim|required');
            $this->form_validation->set_rules('customers_federative_unit','','trim|required');
            $this->form_validation->set_rules('customers_country','','trim|required');
            $this->form_validation->set_rules('customers_defaulting','','trim|required');
            $this->form_validation->set_rules('customers_types_company_id','','trim|required');
            $this->form_validation->set_rules('customers_description','','trim|required|max_length[500]');
            if($this->form_validation->run()){            
                $data = elements(
                    array(         
                        'customers_social_reason',   
                        'customers_fantasy_name',
                        'customers_cnpj',
                        'customers_state_registration',
                        'customers_email',
                        'customers_telephone_fix',
                        'customers_telephone',
                        'customers_cep',
                        'customers_address',
                        'customers_address_number',
                        'customers_address_complement',
                        'customers_neighborhood',
                        'customers_city',
                        'customers_federative_unit',
                        'customers_country',
                        'customers_defaulting',
                        'customers_number_pumps',
                        'customers_automations_id',
                        'customers_types_identifier_id',
                        'customers_pumps_brand_id',
                        'customers_types_company_id',                     
                        'customers_comments',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('customers', $data, array('customers_id'=>$customer_id));
                redirect('customers');
            }else{                
                $data = array(
                    'title' => 'Atualizar cadastro cliente',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'customer' => $this->core_model->get_by_id('customers', array('customers_id' => $customer_id)),
                );
            $this->load->view('layout/header', $data);
            $this->load->view('customers/edit');
            $this->load->view('layout/footer');
            }
        }
    }

    public function add(){
        $this->form_validation->set_rules('customers_social_reason','','trim|required');
        $this->form_validation->set_rules('customers_fantasy_name','','trim|required');
        $this->form_validation->set_rules('customers_cnpj','','trim|required|exact_length[18]');
        $this->form_validation->set_rules('customers_state_registration','','trim|required');
        $this->form_validation->set_rules('customers_email','','trim|required|valid_email');
        $this->form_validation->set_rules('customers_telephone','','trim|required');
        $this->form_validation->set_rules('customers_address','','trim|required');
        $this->form_validation->set_rules('customers_address_number','','trim|required');
        $this->form_validation->set_rules('customers_neighborhood','','trim|required');
        $this->form_validation->set_rules('customers_city','','trim|required');
        $this->form_validation->set_rules('customers_federative_unit','','trim|required');
        $this->form_validation->set_rules('customers_country','','trim|required');
        $this->form_validation->set_rules('customers_defaulting','','trim|required');
        $this->form_validation->set_rules('customers_types_company_id','','trim|required');
        $this->form_validation->set_rules('customers_description','','trim|required|min_length[5]|max_length[500]');
        if($this->form_validation->run()){
            $data = elements(
                array(                                           
                    'customers_social_reason',   
                    'customers_fantasy_name',
                    'customers_cnpj',
                    'customers_state_registration',
                    'customers_email',
                    'customers_telephone_fix',
                    'customers_telephone',
                    'customers_cep',
                    'customers_address',
                    'customers_address_number',
                    'customers_address_complement',
                    'customers_neighborhood',
                    'customers_city',
                    'customers_federative_unit',
                    'customers_country',
                    'customers_comments',
                    'customers_defaulting',
                    'customers_number_pumps',
                    'customers_automations_id',
                    'customers_types_identifier_id',
                    'customers_pumps_brand_id',
                    'customers_types_company_id',
                ),
                $this->input->post()
            );

            $data['customers_federative_unit'] = strtoupper($this->input->post('customers_federative_unit'));

            echo '<pre>';
            print_r($data);
            exit();

            //$data = html_escape($data);
            $this->core_model->insert('customers', $data);
            redirect('customers');
        }else{
            $data = array(
                'title' => 'Cadastrar cliente',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                
            );
            $this->load->view('layout/header',$data);
            $this->load->view('customers/add');
            $this->load->view('layout/footer');           
        }
    }

    public function show($customer_id = NULL){
        if(!$customer_id || !$this->core_model->get_by_id('customers', array('id' => $customer_id))){
            $this->session->set_flashdata('error', 'Cliente não cadastrado.');
            redirect('customers');
        }else {
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'social_reason',   
                        'fantasy_name',
                        'cnpj',
                        'state_registration',
                        'email',
                        'telephone_fix',
                        'telephone',
                        'cep',
                        'address',
                        'address_number',
                        'address_complement',
                        'neighborhood',
                        'city',
                        'federative_unit',
                        'country',
                        'defaulting',
                        'number_pumps',
                        'automations_id',
                        'types_identifier_id',
                        'pumps_brand_id',
                        'types_company_id',                     
                        'comments',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                //$this->core_model->update('customers', $data, array('id'=>$customer_id));
                redirect('customers');
            }else{                
                $data = array(
                    'title' => 'Mostrar cliente',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'customer' => $this->core_model->get_by_id('customers', array('id' => $customer_id)),
                );
            $this->load->view('layout/header', $data);
            $this->load->view('customers/show');
            $this->load->view('layout/footer');
            }
        }
    }


    public function show_all_customers_calleds(){
        $data = array(
            'title' => 'Clientes cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',              
            ),
            'customers' => $this->core_model->get_all('customers'),
        );
        //echo '<pre>';
        //print_r($data['endCustomers']);
        //exit();
        $this->load->view('layout/header',$data);
        $this->load->view('calleds/showAllCustomersCalleds');
        $this->load->view('layout/footer');
    }

    public function newCalled($customer_id = NULL)
    {        
        $this->form_validation->set_rules('calleds_start','','trim|required');
        $this->form_validation->set_rules('calleds_description','','trim|required|min_length[5]|max_length[500]');
        $this->form_validation->set_rules('calleds_status_id','','trim|required');
        $this->form_validation->set_rules('calleds_users_id','','trim|required');
        $this->form_validation->set_rules('calleds_call_types_id','','trim|required');
        $this->form_validation->set_rules('calleds_priorities_id','','trim|required');
        $this->form_validation->set_rules('calleds_customers_end_id','','trim|required');
        $this->form_validation->set_rules('calleds_customers_partner_id','','trim|required');
        $this->form_validation->set_rules('calleds_automations_id','','trim|required');
        $this->form_validation->set_rules('calleds_types_identifier_id','','trim|required');        
        if($this->form_validation->run()){
            $data = elements(
                array(                                           
                    'calleds_start',
                    'calleds_end',
                    'calleds_description',
                    'calleds_status_id',
                    'calleds_users_id',
                    'calleds_call_types_id',
                    'calleds_priorities_id',
                    'calleds_customers_end_id',
                    'calleds_customers_partner_id',
                    'calleds_automations_id',
                    'calleds_types_identifier_id',
                ),
                $this->input->post()
            );

            $data = html_escape($data);
            $this->core_model->insert('calleds', $data);
            redirect('calleds');
        }else{
            $data = array(
                'title' => 'Cadastrar chamado de serviço',
                'styles' => array(
                    'vendor/jquery/jquery-ui.min.css', 
                ),
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                    'vendor/jquery/jquery-ui.min.js', 
                    'vendor/js/autocomplete.js',
                ),    
                'customer' => $this->core_model->get_by_id('customers', array('customers_id' => $customer_id)),
                'call_types' => $this->core_model->get_all('call_types'),
                'priorities' => $this->core_model->get_all('priorities'),
                'users' => $this->core_model->get_all('users'),
                'automations' => $this->core_model->get_all('automations'),
                'types_identifier' => $this->core_model->get_all('types_identifier'),
                'status' => $this->core_model->get_all('status'),
            );
            $this->load->view('layout/header',$data);
            $this->load->view('calleds/addWithCustomer');
            $this->load->view('layout/footer');           
        }
    }

    public function relatedCalleds($customer_id = NULL){
        $data = array(
            'title' => 'Chamados relacionados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
                'vendor/jquery/jquery-ui.min.css', 
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js', 
                'vendor/jquery/jquery-ui.min.js', 
                'vendor/js/autocomplete.js',             
            ),
            'calleds' => $this->customers_model->getAllCalledsToCustomer($customer_id),
            //'calledsAll' => $this->calleds_model->get_all(),
        );
        //echo '<pre>';
        //print_r($data['endCustomers']);
        //exit();
        $this->load->view('layout/header',$data);
        $this->load->view('customers/showRelatedCalleds');
        $this->load->view('layout/footer');
    }
    /*
    public function check_cnpj($customer_cnpj){    
        $customer_id = $this->input->post('customer_id');        
        if($this->core_model->get_by_id('customers', array('customers_cnpj' => $customer_cnpj, 'customers_id !=' => $customer_id))){        
            $this->form_validation->set_message('callback_valid_cnpj','Esse CNPJ já está cadastrado.');
            return FALSE;
        }else{
            return TRUE;
        }
    }   
    public function valid_cnpj($cnpj) {

        // Verifica se um número foi informado
        if (empty($cnpj)) {
            $this->form_validation->set_message('valid_cnpj', 'Por favor digite um CNPJ válido');
            return false;
        }

        if ($this->input->post('customer_id')) {

            $customer_id = $this->input->post('customer_id');

            if ($this->core_model->get_by_id('customers', array('customers_id !=' => $customer_id, 'customers_cnpj' => $cnpj))) {
                $this->form_validation->set_message('valid_cnpj', 'Esse CNPJ já existe');
                return FALSE;
            }
        }

        // Elimina possivel mascara
        $cnpj = preg_replace("/[^0-9]/", "", $cnpj);
        $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);


        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cnpj) != 14) {
            $this->form_validation->set_message('valid_cnpj', 'Por favor digite um CNPJ válido');
            return false;
        }

        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cnpj == '00000000000000' ||
                $cnpj == '11111111111111' ||
                $cnpj == '22222222222222' ||
                $cnpj == '33333333333333' ||
                $cnpj == '44444444444444' ||
                $cnpj == '55555555555555' ||
                $cnpj == '66666666666666' ||
                $cnpj == '77777777777777' ||
                $cnpj == '88888888888888' ||
                $cnpj == '99999999999999') {
            $this->form_validation->set_message('valid_cnpj', 'Por favor digite um CNPJ válido');
            return false;

            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            $j = 5;
            $k = 6;
            $soma1 = "";
            $soma2 = "";

            for ($i = 0; $i < 13; $i++) {

                $j = $j == 1 ? 9 : $j;
                $k = $k == 1 ? 9 : $k;

                //$soma2 += ($cnpj{$i} * $k);

                //$soma2 = intval($soma2) + ($cnpj{$i} * $k); //Para PHP com versão < 7.4
                $soma2 = intval($soma2) + ($cnpj[$i] * $k); //Para PHP com versão > 7.4

                if ($i < 12) {
                    //$soma1 = intval($soma1) + ($cnpj{$i} * $j); //Para PHP com versão < 7.4
                    $soma1 = intval($soma1) + ($cnpj[$i] * $j); //Para PHP com versão > 7.4
                }

                $k--;
                $j--;
            }

            $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
            $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;

            if (!($cnpj(12) == $digito1) and ( $cnpj(13) == $digito2)) {
                $this->form_validation->set_message('valid_cnpj', 'Por favor digite um CNPJ válido');
                return false;
            } else {
                return true;
            }
        }
    }    
    */
}