<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Typesidentifier extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
    }

    public function index(){

        $data = array(
            'title' => 'Tipos de identificadores cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'types_identifier' => $this->core_model->get_all('types_identifier'),
        );

        /*
        echo '<pre>';
        print_r($data['types_identifier']);
        exit();
        */
        
        $this->load->view('layout/header',$data);
        $this->load->view('typesidentifier/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($type_identifier_id = NULL){
        if(!$type_identifier_id || !$this->core_model->get_by_id('types_identifier', array('types_identifier_id' => $type_identifier_id))){
            $this->session->set_flashdata('error','Tipo de identificador não encontrada.');
            redirect('typesidentifier');
        } else {
            $this->form_validation->set_rules('types_identifier_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'types_identifier_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('types_identifier', $data, array('types_identifier_id'=>$type_identifier_id));

                redirect('typesidentifier');
            }else{

                $data = array(
                    'title' => ' Atualizar tipo de identificador',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'type_identifier' => $this->core_model->get_by_id('types_identifier', array('types_identifier_id' => $type_identifier_id)),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('typesidentifier/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('types_identifier_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'types_identifier_description',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('types_identifier', $data);
            redirect('typesidentifier');
        }else{
            $data = array(
                'title' => 'Cadastrar tipo de identificador',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('typesidentifier/add');
            $this->load->view('layout/footer');           
        }
    }

    public function delete($type_identifier_id = NULL){
        if(!$type_identifier_id || !$this->core_model->get_by_id('types_identifier', array('types_identifier_id' => $type_identifier_id))){
            $this->session->set_flashdata('error','Tipo de identificador não encontrado.');
            redirect('typesidentifier');
        } else {
            $this->core_model->delete('types_identifier', array('types_identifier_id'=>$type_identifier_id));
            redirect('typesidentifier');
        }
    }
}