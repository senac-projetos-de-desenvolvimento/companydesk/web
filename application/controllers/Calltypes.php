<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Calltypes extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
       // $this->load->model('activities_model');
    }

    public function index(){
        $data = array(
            'title' => 'Tipos de chamados cadastradas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'call_types' => $this->core_model->get_all('call_types'),            
        );

        //echo '<pre>';
        //print_r($data['call_types']);
        //exit();
        
        $this->load->view('layout/header',$data);
        $this->load->view('calltypes/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($call_type_id = NULL){
        if(!$call_type_id || !$this->core_model->get_by_id('call_types', array('call_types_id' => $call_type_id))){
            $this->session->set_flashdata('error','Tipo de chamado não encontrado.');
            redirect('call_types');
        } else {
            $this->form_validation->set_rules('call_types_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'call_types_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('call_types', $data, array('call_types_id'=>$call_type_id));

                redirect('calltypes');
            }else{

                $data = array(
                    'title' => 'Atualizar tipo de chamado',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'call_type' => $this->core_model->get_by_id('call_types', array('call_types_id' => $call_type_id)),                    
                );
                $this->load->view('layout/header',$data);
                $this->load->view('calltypes/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('call_types_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'call_types_description',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('call_types', $data);
            redirect('calltypes');
        }else{
            $data = array(
                'title' => 'Cadastrar tipo de chamado',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('calltypes/add');
            $this->load->view('layout/footer');           
        }
    }

    public function delete($call_type_id = NULL){
        if(!$call_type_id || !$this->core_model->get_by_id('call_types', array('call_types_id' => $call_type_id))){
            $this->session->set_flashdata('error','Tipo de chamado não encontrado.');
            redirect('calltypes');
        } else {
            $this->core_model->delete('call_types', array('call_types_id'=>$call_type_id));
            redirect('calltypes');
        }
    }
}