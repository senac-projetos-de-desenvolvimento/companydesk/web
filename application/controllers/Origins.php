<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Origins extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
       // $this->load->model('activities_model');
    }

    public function index(){

        $data = array(
            'title' => 'Origens cadastradas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'origins' => $this->core_model->get_all('origins'),
        );

        //echo '<pre>';
        //print_r($data['origins']);
        //exit();
        
        $this->load->view('layout/header',$data);
        $this->load->view('origins/index');
        $this->load->view('layout/footer');
    }

    public function edit($origin_id = NULL){
        if(!$origin_id || !$this->core_model->get_by_id('origins', array('origins_id' => $origin_id))){
            $this->session->set_flashdata('error','Tipo de origem não encontrado.');
            redirect('origins');
        } else {
            $this->form_validation->set_rules('origins_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'origins_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('origins', $data, array('origins_id'=>$origin_id));

                redirect('origins');
            }else{
                $data = array(
                    'title' => ' Atualizar tipo de origem',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'origin' => $this->core_model->get_by_id('origins', array('origins_id' => $origin_id)),                    
                );
                $this->load->view('layout/header',$data);
                $this->load->view('origins/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('origins_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'origins_description',
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('origins', $data);
            redirect('origins');
        }else{
            $data = array(
                'title' => 'Cadastrar tipo de chamado',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('origins/add');
            $this->load->view('layout/footer');           
        }
    }
    public function delete($origin_id = NULL){
        if(!$origin_id || !$this->core_model->get_by_id('origins', array('origins_id' => $origin_id))){
            $this->session->set_flashdata('error','Origem não encontrada.');
            redirect('origins');
        } else {
            $this->core_model->delete('origins', array('origins_id'=>$origin_id));
            redirect('origins');
        }
    }
}