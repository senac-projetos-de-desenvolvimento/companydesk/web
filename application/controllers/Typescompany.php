<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Typescompany extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
    }

    public function index(){

        $data = array(
            'title' => 'Tipos de empresas cadastradas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'types_company' => $this->core_model->get_all('types_company'),
        );

        /*
        echo '<pre>';
        print_r($data['types_company']);
        exit();
        */
        
        $this->load->view('layout/header',$data);
        $this->load->view('typescompany/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($type_company_id = NULL){
        if(!$type_company_id || !$this->core_model->get_by_id('types_company', array('types_company_id' => $type_company_id))){
            $this->session->set_flashdata('error','Tipo de empresa não encontrada.');
            redirect('typescompany');
        } else {
            $this->form_validation->set_rules('types_company_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'types_company_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('types_company', $data, array('types_company_id'=>$type_company_id));

                redirect('typescompany');
            }else{

                $data = array(
                    'title' => ' Atualizar tipo de empresa',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'type_company' => $this->core_model->get_by_id('types_company', array('types_company_id' => $type_company_id)),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('typescompany/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('types_company_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'types_company_description',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('types_company', $data);
            redirect('typescompany');
        }else{
            $data = array(
                'title' => 'Cadastrar tipo de empresa',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('typescompany/add');
            $this->load->view('layout/footer');           
        }
    }
    public function delete($type_company_id = NULL){
        if(!$type_company_id || !$this->core_model->get_by_id('types_company', array('types_company_id' => $type_company_id))){
            $this->session->set_flashdata('error','Tipo de empresa não encontrado.');
            redirect('typescompany');
        } else {
            $this->core_model->delete('types_company', array('types_company_id'=>$type_company_id));
            redirect('typescompany');
        }
    }
}