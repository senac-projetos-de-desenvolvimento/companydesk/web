<?php
defined('BASEPATH') OR exit('Ação não permitida.');
class Calleds extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }
        $this->load->model('core_model');
        $this->load->model('activities_model');
        $this->load->model('calleds_model');
    }

    public function index(){
        $data = array(
            'title' => 'Chamados cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
                'vendor/jquery/jquery-ui.min.css', 
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js', 
                'vendor/jquery/jquery-ui.min.js',
                'vendor/js/autocomplete.js',                
            ),
            'calleds' => $this->calleds_model->get_all(),
            //'calleds' => $this->calleds_model->get_customer_partner(),
        );

        //echo'<pre>';
        //print_r($data['calleds']);
        //exit();

        $this->load->view('layout/header',$data);
        $this->load->view('calleds/index');
        $this->load->view('layout/footer');
    }

    public function edit($called_id = NULL){
        if(!$called_id || !$this->core_model->get_by_id('calleds', array('calleds_id' => $called_id))){
            $this->session->set_flashdata('error','Chamado não encontrado.');
            redirect('calleds');
        } else {
            $this->form_validation->set_rules('calleds_description','','trim|required|min_length[5]|max_length[500]');
            $this->form_validation->set_rules('calleds_status_id','','trim|required');
            $this->form_validation->set_rules('calleds_priorities_id','','trim|required');
            $this->form_validation->set_rules('calleds_customers_end_id','','trim|required');
            $this->form_validation->set_rules('calleds_customers_partner_id','','trim|required');
            $this->form_validation->set_rules('calleds_automations_id','','trim|required');
            $this->form_validation->set_rules('calleds_types_identifier_id','','trim|required');  
            if($this->form_validation->run()){
                $data = elements(
                    array(
                        'calleds_description',
                        'calleds_status_id',
                        'calleds_priorities_id',
                        'calleds_customers_end_id',
                        'calleds_customers_partner_id',
                        'calleds_automations_id',
                        'calleds_types_identifier_id',
                    ),
                    $this->input->post()
                );
                $data['calleds_users_id'] = $this->ion_auth->get_user_id();
                $data = html_escape($data);
                $this->core_model->update('calleds', $data, array('calleds_id'=>$called_id));
                redirect('calleds');
            }else{

                $data = array(
                    'title' => 'Atualizar chamado',
                    'styles' => array(
                        'vendor/jquery/jquery-ui.min.css', 
                    ),
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                        'vendor/jquery/jquery-ui.min.js', 
                        'vendor/js/autocomplete.js',
                    ),                                        
                    'called' => $this->core_model->get_by_id('calleds', array('calleds_id' => $called_id)),
                    'customers' => $this->core_model->get_all('customers'),
                    'status' => $this->core_model->get_all('status'),
                    'priorities' => $this->core_model->get_all('priorities'),
                    'automations' => $this->core_model->get_all('automations'),
                    'types_identifier' => $this->core_model->get_all('types_identifier'),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('calleds/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){
        $this->form_validation->set_rules('calleds_start','','trim|required');
        $this->form_validation->set_rules('calleds_status_id','','trim|required');
        // $this->form_validation->set_rules('calleds_users_id','','trim|required');
        // $this->form_validation->set_rules('calleds_call_types_id','','trim|required');
        $this->form_validation->set_rules('calleds_priorities_id','','trim|required');
        $this->form_validation->set_rules('calleds_customers_end_id','','trim|required');
        $this->form_validation->set_rules('calleds_customers_partner_id','','trim|required');
        $this->form_validation->set_rules('calleds_automations_id','','trim|required');
        $this->form_validation->set_rules('calleds_types_identifier_id','','trim|required');    
        // redirect('/calleds/listagem');

        if($this->form_validation->run()){            
            $data = elements(
                array(                                           
                    'calleds_start',
                    'calleds_end',
                    'calleds_description',
                    'calleds_status_id',
                    // 'calleds_call_types_id',
                    'calleds_priorities_id',
                    'calleds_customers_end_id',
                    'calleds_customers_partner_id',
                    'calleds_automations_id',
                    'calleds_types_identifier_id',
                ),
                $this->input->post()                
            );
            $data['calleds_users_id'] = $this->ion_auth->get_user_id();
            if($data['calleds_end'] == ""){
                $data['calleds_end'] = NULL;                   
            }
            $data = html_escape($data);

            if($this->core_model->insert('calleds',$data)){
                $this->session->set_flashdata('success', 'Dados salvos com sucesso.');
            }else{
                $this->session->set_flashdata('error', 'Erro ao salvar dados.');
            }
            redirect('calleds');
        }else{
           $data = array(
                'title' => 'Cadastrar chamado de serviço',
                'styles' => array(
                    'vendor/jquery/jquery-ui.min.css', 
                ),
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                    'vendor/jquery/jquery-ui.min.js', 
                    'vendor/js/autocomplete.js',
                ),    
                // 'call_types' => $this->core_model->get_all('call_types'),
                'priorities' => $this->core_model->get_all('priorities'),
                'users' => $this->core_model->get_all('users'),
                'automations' => $this->core_model->get_all('automations'),
                'types_identifier' => $this->core_model->get_all('types_identifier'),
                'status' => $this->core_model->get_all('status'),
            );
            $this->load->view('layout/header',$data);
            $this->load->view('calleds/add');
            $this->load->view('layout/footer');
        }
    }

    public function relatedActivities($called_id = NULL){
        $data = array(
            'title' => 'Atividades relacionadas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',              
            ),
            'activities' => $this->calleds_model->getAllActivitiesToCalled($called_id),
        );
        //echo '<pre>';
        //print_r($data['endCustomers']);
        //exit();
        $this->load->view('layout/header',$data);
        $this->load->view('calleds/showRelatedActivities');
        $this->load->view('layout/footer');
    }

    public function showDescription($called_id){                   
        $data = array(
            'title' => 'Mostrar Chamado',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'called' => $this->core_model->get_by_id('calleds', array('calleds_id' => $called_id)),
            'status' => $this->core_model->get_all('status'),
            'users' => $this->core_model->get_all('users'),
            // 'call_types' => $this->core_model->get_all('call_types'),
            'priorities' => $this->core_model->get_all('priorities'),
            'customers' => $this->core_model->get_all('customers'),
            'automations' => $this->core_model->get_all('automations'),
            'types_identifier' => $this->core_model->get_all('types_identifier'), 
            'activitiesCalled' => $this->activities_model->getAllActivitiesToCalled($called_id),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('calleds/showDescription');
        $this->load->view('layout/footer');
    }

    public function showCustomer($customer_id){
        $data = array(
            'title' => 'Mostrar Cliente',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'customer' => $this->core_model->get_by_id('customers', array('customers_id' => $customer_id)),
            'automations' => $this->core_model->get_all('automations'),
            'types_identifier' => $this->core_model->get_all('types_identifier'), 
            'pumps_brand' => $this->core_model->get_all('pumps_brand'),
            'types_company' => $this->core_model->get_all('types_company'),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('calleds/showCustomer');
        $this->load->view('layout/footer');
    }

    public function addActivityWithCalled($called_id){
        $this->form_validation->set_rules('activities_description','','trim|required|min_length[5]|max_length[500]');        
        $this->form_validation->set_rules('activities_start','','trim|required');       
        $this->form_validation->set_rules('activities_users_id','','trim|required'); 
        $this->form_validation->set_rules('activities_customers_partner_id','','trim|required');
        $this->form_validation->set_rules('activities_customers_end_id','','trim|required');
        $this->form_validation->set_rules('activities_origins_id','','trim|required');
        $this->form_validation->set_rules('activities_calleds_id','','trim|required');
        $this->form_validation->set_rules('activities_priorities_id','','trim|required');
        $this->form_validation->set_rules('activities_automations_id','','trim|required');
        $this->form_validation->set_rules('activities_types_identifier_id','','trim|required');
        $this->form_validation->set_rules('activities_call_types_id','','trim|required');

        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'activities_description',
                    'activities_start',
                    'activities_end',
                    'activities_attachments',
                    'activities_users_id',
                    'activities_customers_partner_id',
                    'activities_customers_end_id',                    
                    'activities_origins_id',
                    'activities_calleds_id',
                    'activities_priorities_id',
                    'activities_automations_id',
                    'activities_types_identifier_id',
                    'activities_call_types_id',
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('activities', $data);
            redirect('activities');
        }else{
            $data = array(
                'title' => 'Cadastrar atividades',
                'styles' => array(
                    'vendor/jquery/jquery-ui.min.css', 
                ),
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                    'vendor/jquery/jquery-ui.min.js', 
                    'vendor/js/autocomplete.js',
                ),
                'called' => $this->core_model->get_by_id('calleds', array('calleds_id' => $called_id)),  
                'users' => $this->core_model->get_all('users'),
                'call_types' => $this->core_model->get_all('call_types'),
                'origins' => $this->core_model->get_all('origins'),
                'priorities' => $this->core_model->get_all('priorities'),
                'automations' => $this->core_model->get_all('automations'),
                'types_identifier' => $this->core_model->get_all('types_identifier'),
                'customers' => $this->core_model->get_all('customers'),              
            );
            $this->load->view('layout/header',$data);
            $this->load->view('calleds/addActivityWithCalled');
            $this->load->view('layout/footer');           
        }
    }

    public function verifyCalledWithCustomer($customer_id = NULL){
        
        echo'<pre>';
        print_r($this->input->post('calleds_customers_end_id'));
        exit();
    }

    public function searchCustomersEnd(){
        //$this->calleds_model->getCustomersEndAutoComplete()
        $rows = $this->calleds_model->getCustomersEndAutoComplete($_GET['term']);
        if(count($rows) > 0){
            foreach ($rows as $row) {
                $dados[] = array('label'		 =>		$row['customers_social_reason'],
                                    'id'	 	 =>		$row['customers_id'],
                                    'value'		 =>		$row['customers_social_reason']	);
            }
            echo json_encode($dados);
        }
    }

    public function searchCustomersPartner(){
        //$this->calleds_model->getCustomersPartnerAutoComplete()
        $rows = $this->calleds_model->getCustomersPartnerAutoComplete($_GET['term']);
        if(count($rows) > 0){
            foreach ($rows as $row) {
                $dados[] = array('label'		 =>		$row['customers_social_reason'],
                                    'id'	 	 =>		$row['customers_id'],
                                    'value'		 =>		$row['customers_social_reason']	);
            }
            echo json_encode($dados);
        }
    }
}
?>