<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Status extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
    }

    public function index(){

        $data = array(
            'title' => 'Status cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'status' => $this->core_model->get_all('status'),
        );

        /*
        echo '<pre>';
        print_r($data['status']);
        exit();
        */
        
        $this->load->view('layout/header',$data);
        $this->load->view('status/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($statu_id = NULL){
        if(!$statu_id || !$this->core_model->get_by_id('status', array('status_id' => $statu_id))){
            $this->session->set_flashdata('error','Status não encontrado.');
            redirect('status');
        } else {
            $this->form_validation->set_rules('status_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'status_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('status', $data, array('status_id'=>$statu_id));

                redirect('status');
            }else{

                $data = array(
                    'title' => ' Atualizar status',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'statu' => $this->core_model->get_by_id('status', array('status_id' => $statu_id)),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('status/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('status_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'status_description',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('status', $data);
            redirect('status');
        }else{
            $data = array(
                'title' => 'Cadastrar status',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('status/add');
            $this->load->view('layout/footer');           
        }
    }
    public function delete($statu_id = NULL){
        if(!$statu_id || !$this->core_model->get_by_id('status', array('status_id' => $statu_id))){
            $this->session->set_flashdata('error','Tipo de status não encontrado.');
            redirect('status');
        } else {
            $this->core_model->delete('status', array('status_id'=>$statu_id));
            redirect('status');
        }
    }
}