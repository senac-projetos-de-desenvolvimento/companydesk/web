<?php
    defined('BASEPATH') OR exit('Ação não permitida');

    class Login extends CI_Controller{
        public function index(){
            $data = array(
                'title' => "Login",
            );

            $this->load->view('layout/header', $data);
            $this->load->view('login/index');
            $this->load->view('layout/footer');
            
        }

        public function authentication(){
            $identity = $this->security->xss_clean($this->input->post('email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $remember = FALSE;
            if($this->ion_auth->login($identity, $password, $remember)){
                $this->load->model('home_model');
                //$this->sendEmailWelcome();
                $this->sendEmailAlertActivities();
                redirect('home');
            } else {
                $this->session->set_flashdata('error', 'Verifique seu e-mail ou senha.');
                redirect('login');
            }            
        }
        
        public function logout(){
            $this->ion_auth->logout();
            redirect('login');
        }

        private function sendEmailWelcome(){
            $user = $this->ion_auth->user()->row();
            $body = file_get_contents(base_url().'assets/email_templates/welcome.php');
            $body = str_replace("{{NAME}}", $user->first_name, $body);  
            try {
                $result = $this->email
                    ->from('no-reply@companydesk.com.br')
                    ->to($user->email)
                    ->subject('Novo login detectado.')
                    ->message($body)
                    ->send();
                // var_dump($result);
            } catch (Exception $e) {
                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            }
            redirect('home');
        }
        
        function sendEmailAlertActivities(){
            $actHigh = $this->home_model->activitiesHighPriorityWinning();
            if($actHigh->numberActivitiesHigh != 0){
                $user = $this->ion_auth->user()->row();
                $body = file_get_contents(base_url().'assets/email_templates/alertCalleds.php');
                $body = str_replace("{{ACTIVITIES}}",$actHigh->numberActivitiesHigh, $body);
                $body = str_replace("{{NAME}}", $user->first_name, $body);
                try {
                    $result = $this->email
                        ->from('no-reply@companydesk.com.br')
                        ->to($user->email)
                        ->subject('Atividades com prioridade alta vencendo.')
                        ->message($body)
                        ->send();
                    // var_dump($result);
                } catch (Exception $e) {
                    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }
            }
        }
    }