<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('origins'); ?>">Tipos de origens</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_edit_origin">                   
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Tipo de origem</label>
                            <input type="text" class="form-control" name="origins_description"
                                 value="<?php echo $origin->origins_description;?>">
                            <?php echo form_error('origins_description','<small class="form-text text-danger">','</small>')?>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-sm ml-3"><i class="far fa-save"></i>&nbsp;&nbsp;Salvar</button>
                        <a title="Voltar" href="<?php echo base_url('origins');?>"
                            class=" btn btn-success btn-sm float-right ml-3"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;Voltar</a>
                    </div>            
                </form>
            </div>
        </div>
    </div>
</div>