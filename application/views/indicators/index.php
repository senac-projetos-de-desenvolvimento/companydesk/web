<?php $this->load->view('layout/sidebar');?>
<div id="content">
<?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Informações</h1>
        </div>
        <div class="row">
        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Area Chart</h6>
                </div>
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="areaChartMonthCalleds"></canvas>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Chamados por mês</h6>
                </div>
                <div class="card-body">
                    <div class="chart-bar">
                        <canvas id="barChartMonthCalleds"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Chamados por usuário (mês)</h6>
                </div>
                <div class="card-body">
                    <div class="chart-pie pt-4">
                        <canvas id="pieChartMonthCalleds"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var ctx = document.getElementById("barChartMonthCalleds");
    var barChartMonthCalleds = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: <?= $months ?>, //["patrick", "teste", "weee", "April", "May", "June"],
            datasets: [
                {
                    backgroundColor: "#2e59d9",
                    borderColor: "#2e59d9",
                    data: <?= $calleds ?>, // [5000, 6000, 7000, 7841, 9821, 20000],
                }
            ],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
            },
            legend: {
            display: false
            },
            responsive: true,
            cutoutPercentage: 80,
        },
    });

    var ctx = document.getElementById("areaChartMonthCalleds");
    var areaChartMonthCalleds = new Chart(ctx, {
        type: 'line',        
        data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: [0, 10000, 5000, 15000, 10000, 20000, 15000, 25000, 20000, 30000, 25000, 40000],
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
            },
            legend: {
            display: false
            },
            responsive: true,
            cutoutPercentage: 80,
        },
    });

    var ctx = document.getElementById("pieChartMonthCalleds");
    var pieChartMonthCalleds = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: <?= $usersCalleds ?>, //["Direct", "Referral", "Social"],
            datasets: [{
                data: <?= $calledCalleds ?>, //[50, 30, 15],
                backgroundColor: ['#37c2bf', '#3366cc', '#990099','#109618ff', '#ff9900ff', '#dc3912ff'],
                // hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 2,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
            },
            legend: {
                display: true,
                position: 'right',
            },
            responsive: true,
            cutoutPercentage: 80,
        },
    });    
</script>