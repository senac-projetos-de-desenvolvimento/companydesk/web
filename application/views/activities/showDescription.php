<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>

    <div class="container-fluid">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('activities'); ?>">Atividades</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="from_show_activity">
                    <?php foreach($customers as $customer): ?>
                        <?php 
                            if($activity->activities_customers_end_id == $customer->customers_id){
                                $activity_customer_end = $customer->customers_social_reason;
                            }    
                            if($activity->activities_customers_partner_id == $customer->customers_id){
                                $activity_customer_partner = $customer->customers_social_reason;
                            }                  
                        ?>
                    <?php endforeach; ?>   
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Cliente</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="activities_customers_end_id"
                                value="<?php echo $activity_customer_end?>" readonly>
                                <?php echo form_error('activities_customers_end_id','<small class="form-text text-danger">','</small>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Parceiro</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="activities_customers_partner_id"
                                    value="<?php echo $activity_customer_partner;?>" readonly>
                                <?php echo form_error('activities_customers_partner_id','<small class="form-text text-danger">','</small>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="activities_users_id" disabled>
                                <?php foreach($users as $user): ?>
                                   <option value="<?php echo $user->id ?>"  <?php echo ($user->id == $activity->activities_users_id ? 'selected' : '') ?>><?php echo $user->first_name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Origem</label>
                            <select class="custom-select" name="activities_origins_id" disabled>
                                <?php foreach($origins as $origin): ?>
                                    <option value="<?php echo $origin->origins_id ?>" <?php echo ($origin->origins_id == $activity->activities_origins_id ? 'selected' : '') ?> > <?php echo $origin->origins_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>                        
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Chamado</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="activities_calleds_id"
                                    value="<?php echo $activity->activities_calleds_id;?>" readonly>
                                <?php echo form_error('activities_calleds_id','<small class="form-text text-danger">','</small>')?>                               
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Prioridade</label>
                            <select class="custom-select" name="activities_priorities_id" disabled>
                                <?php foreach($priorities as $priority): ?>
                                    <option value="<?php echo $priority->priorities_id ?>" <?php echo ($priority->priorities_id == $activity->activities_priorities_id ? 'selected' : '') ?>> <?php echo $priority->priorities_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Automação</label>
                            <select class="custom-select" name="activities_automations_id" disabled>
                                <?php foreach($automations as $automation): ?>
                                    <option value="<?php echo $automation->automations_id ?>"  <?php echo ($automation->automations_id  == $activity->activities_automations_id ? 'selected' : '') ?> ><?php echo $automation->automations_model ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Identificador</label>
                            <select class="custom-select" name="activities_types_identifier_id" disabled>
                                <?php foreach($types_identifier as $type_identifier): ?>
                                    <option value="<?php echo $type_identifier->types_identifier_id ?>" <?php echo ($type_identifier->types_identifier_id == $activity->activities_types_identifier_id ? 'selected' : '') ?> ><?php echo $type_identifier->types_identifier_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- <div class="col-md-3">
                            <label>Anexos</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="activities_attachments"
                                    value="<?php echo set_value('activities_attachments');?>" readonly>
                                <?php echo form_error('activities_attachments','<small class="form-text text-danger">','</small>')?>                                
                            </div>
                        </div> -->
                        <div class="col-md-3">
                            <label>Status</label>
                            <select class="custom-select" name="activities_status_id" disabled>
                                <?php foreach($status as $statu): ?>
                                    <option value="<?php echo $statu->status_id ?>"<?php echo ($statu->status_id == $activity->activities_status_id ? 'selected' : '') ?> ><?php echo $statu->status_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div> 
                        <div class="col-md-3">
                            <label>Tipo</label>
                            <select class="custom-select" name="activities_call_types_id" disabled>
                                <?php foreach($call_types as $call_type): ?>
                                    <option value="<?php echo $call_type->call_types_id ?>"<?php echo ($call_type->call_types_id == $activity->activities_call_types_id ? 'selected' : '') ?> ><?php echo $call_type->call_types_description ?></option>                                    
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Início</label>
                            <input type="text" class="form-control" name="activities_start" 
                                value="<?php echo $activity->activities_start;?>" readonly>
                            <!-- value é o que vem lá do banco e o user é o que vem do controller-->
                            <?php echo form_error('activities_start','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Final</label>
                            <input type="text" class="form-control" name="activities_end"
                                value="<?php echo $activity->activities_end;?>" readonly>
                            <!-- value é o que vem lá do banco e o user é o que vem do controller-->
                            <?php echo form_error('activities_end','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>                   
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="activities_description"
                                rows="12" readonly><?php echo $activity->activities_description;?></textarea>
                        </div>
                    </div>
                    <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-pen"></i>&nbsp; Atendimentos relacionados</legend>
                        <div class="form-group row mt-3">
                        </div>
                        <div class="table-responsive">
                                <table class="table table-striped table-borderless"> 
                                <thead>
                                    <tr>
                                        <th class="text-center">Id</th>
                                        <th class="text-center">Inicio</th>
                                        <th class="text-center">Final</th>
                                        <th class="text-center">Atividade</th>  
                                        <th class="text-center">Cliente</th>                               
                                        <th class="text-center">Parceiro</th>                                  
                                        <th class="text-center">Usuário</th>
                                        <th class="text-center">Origem</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($attendancesActivity as $attendance):?>
                                    <tr>
                                        <td class="text-center"> <a href="<?php echo base_url('attendances/showDescription/'.$attendance->attendances_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $attendance->attendances_id ?></td>
                                        <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($attendance->attendances_start)) ?></td>
                                        <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($attendance->attendances_end)) ?></td>
                                        <td class="text-center"> <a href="<?php echo base_url('attendances/showActivity/'.$attendance->attendances_activities_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $attendance->attendances_activities_id ?></td>
                                        <td class="text-center"> <a href="<?php echo base_url('activities/showCustomer/'.$attendance->attendances_customers_end_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $attendance->attendances_customers_end ?></td>
                                        <td class="text-center"> <a href="<?php echo base_url('activities/showCustomer/'.$attendance->attendances_customers_partner_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $attendance->attendances_customers_partner ?></td> 
                                        <td class="text-center"> <?php echo $attendance->attendances_user ?></td>
                                        <td class="text-center"> <?php echo $attendance->attendances_origins ?></td>
                                    </tr>                           
                                    <?php endforeach; ?>
                                </tbody>
                                </table>
                            </div>
                        </fieldset>
                    <div class="form-group row">
                        <a title="Voltar" href="<?php echo base_url('calleds/showDescription/'.$activity->activities_calleds_id);?>"
                            class=" btn btn-success btn-sm float-right ml-3 mt-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                        <?php if($activity->activities_status_id != 2){ ?>
                                <a title="Editar" href="<?php echo base_url('activities/edit/'.$activity->activities_id);?>"
                                    class=" btn btn-primary btn-sm ml-3 mt-3"><i class="fas fa-edit"></i>&nbsp; Editar</a>
                                <a title="Novo atendimento" href="<?php echo base_url('attendances/addWithActivity/'.$activity->activities_id);?>"
                                    class=" btn btn-primary btn-sm float-right ml-3 mt-3"><i class="fas fa-plus-circle"></i>&nbsp; Novo atendimento</a>
                        <?php } ?>                  
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>