<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('calleds'); ?>">Chamados de serviço</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_show_called">
                    <?php foreach($customers as $customer): ?>
                        <?php 
                            if($called->calleds_customers_end_id == $customer->customers_id){
                                $called_customer_end = $customer->customers_social_reason;
                            }    
                            if($called->calleds_customers_partner_id == $customer->customers_id){
                                $called_customer_partner = $customer->customers_social_reason;
                            }                  
                        ?>
                    <?php endforeach; ?>  
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Cliente</label>
                            <div class=" input-group">
                                <input type="text" class="form-control" name="calleds_customers_end_id"
                                    value="<?php echo $called_customer_end;?>" readonly>
                                <?php echo form_error('calleds_customers_end_id','<small class="form-text text-danger">','</small>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Parceiro</label>
                            <div class=" input-group">
                                <input type="text" class="form-control" name="calleds_customers_partner_id"
                                    value="<?php echo $called_customer_partner;?>" readonly>
                                <?php echo form_error('calleds_customers_partner_id','<small class="form-text text-danger">','</small>')?>                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de chamado</label>
                            <select class="custom-select" name="calleds_call_types_id" disabled>
                                <?php foreach($call_types as $call_type): ?>
                                    <option value="<?php echo $call_type->call_types_id ?>"<?php echo ($call_type->call_types_id == $called->calleds_call_types_id ? 'selected' : '') ?>><?php echo $call_type->call_types_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">                            
                            <label>Prioridade</label>
                            <select class="custom-select" name="calleds_priorities_id" disabled>
                                <?php foreach($priorities as $priority): ?>
                                    <option value="<?php echo $priority->priorities_id ?>"<?php echo ($priority->priorities_id == $called->calleds_priorities_id ? 'selected' : '') ?>><?php echo $priority->priorities_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="calleds_users_id" disabled>
                                <?php foreach($users as $user): ?>
                                    <option value="<?php echo $user->id ?>" <?php echo ($user->id == $called->calleds_users_id ? 'selected' : '') ?> ><?php echo $user->first_name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Automação</label>
                            <select class="custom-select" name="calleds_automations_id" disabled>
                                <?php foreach($automations as $automation): ?>
                                    <option value="<?php echo $automation->automations_id ?>"<?php echo ($automation->automations_id == $called->calleds_automations_id ? 'selected' : '') ?>><?php echo $automation->automations_model ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de identificador</label>
                            <select class="custom-select" name="calleds_types_identifier_id" disabled>
                                <?php foreach($types_identifier as $type_identifier): ?>
                                    <option value="<?php echo $type_identifier->types_identifier_id ?>"<?php echo ($type_identifier->types_identifier_id == $called->calleds_types_identifier_id ? 'selected' : '') ?>><?php echo $type_identifier->types_identifier_description ?></option>
                                <?php endforeach; ?>
                            </select>                            
                        </div>
                        <div class="col-md-3">
                            <label>Status</label>
                            <select class="custom-select" name="calleds_status_id" disabled>
                                <?php foreach($status as $statu): ?>
                                    <option value="<?php echo $statu->status_id ?>"><?php echo $statu->status_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>              
                    <div class="form-group row">                        
                        <div class="col-md-3">
                            <label>Início</label>
                            <input type="text" class="form-control" name="calleds_start" 
                                value="<?php echo $called->calleds_start;?>" readonly>
                            <?php echo form_error('calleds_start','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Final</label>
                            <input type="text" class="form-control" name="calleds_end" 
                                value="<?php echo $called->calleds_end;?>" readonly>
                            <?php echo form_error('calleds_end','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>                   
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="calleds_description"
                                rows="12" readonly><?php echo $called->calleds_description;?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <a title="Voltar" href="<?php echo base_url('calleds');?>"
                        class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>