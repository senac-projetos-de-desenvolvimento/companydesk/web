<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('activities'); ?>">Atividades</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_edit_activity">                   
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Cliente</label>
                                <input type="text" class="form-control" id="activities_customers_end_id_ac"
                                    value="<?php echo $activity->activities_customers_end_id;?>" readonly>
                                <?php echo form_error('activities_customers_end_id','<small class="form-text text-danger">','</small>')?>
                                <input type="hidden" name="calleds_customers_end_id" id="calleds_customers_end_id">
                        </div>
                        <div class="col-md-3">
                            <label>Parceiro</label>
                                <input type="text" class="form-control" id="activities_customers_partner_id_ac"
                                    value="<?php echo $activity->activities_customers_partner_id;?>" readonly>
                                <?php echo form_error('activities_customers_partner_id','<small class="form-text text-danger">','</small>')?>
                                <input type="hidden" name="activities_customers_partner_id" id="activities_customers_partner_id">                           
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="activities_users_id" disabled>
                                    <?php $user = $this->ion_auth->user()->row(); ?>
                                    <option value="<?php echo $user->id ?>"><?php echo $user->first_name;?></option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Chamado</label>
                                <input type="text" class="form-control" name="activities_calleds_id" value="<?php echo $activity->activities_calleds_id;?>" disabled>
                                <?php echo form_error('activities_calleds_id','<small class="form-text text-danger">','</small>')?>                                                            
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Prioridade</label>
                                <select class="custom-select" name="activities_priorities_id">
                                    <?php var_dump($activity->activities_priorities_id)?>
                                    <?php foreach($priorities as $prioritie):?>
                                        <option value="<?php echo $prioritie->priorities_id ?>"  <?php echo ($prioritie->priorities_id == $activity->activities_priorities_id ? 'selected' : '') ?>><?php echo $prioritie->priorities_description ?></option>
                                    <?php endforeach;?>
                                </select>
                        </div>
                        <div class="col-md-3">
                            <label>Automação</label>
                            <select class="custom-select" name="activities_automations_id">
                                    <?php foreach($automations as $automation):?>
                                        <option value="<?php echo $automation->automations_id ?>"  <?php echo ($automation->automations_id == $activity->activities_automations_id ? 'selected' : '') ?>><?php echo $automation->automations_model ?></option>
                                    <?php endforeach;?>
                                </select>                         
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de identificador</label>
                            <select class="custom-select" name="activities_types_identifier_id">
                                    <?php foreach($types_identifier as $type_identifier):?>
                                        <option value="<?php echo $type_identifier->types_identifier_id ?>"  <?php echo ($type_identifier->types_identifier_id == $activity->activities_types_identifier_id ? 'selected' : '') ?>><?php echo $type_identifier->types_identifier_description ?></option>
                                    <?php endforeach;?>
                                </select>
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de atividade</label>
                                <select class="custom-select" name="activities_call_types_id">
                                    <?php foreach($call_types as $call_type):?>
                                        <option value="<?php echo $call_type->call_types_id ?>"  <?php echo ($call_type->call_types_id == $activity->activities_call_types_id ? 'selected' : '') ?>><?php echo $call_type->call_types_description ?></option>
                                    <?php endforeach;?>
                                </select>                                                     
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Status</label>
                            <select class="custom-select" name="activities_status_id">
                                <?php foreach($status as $statu):?>
                                    <option value="<?php echo $statu->status_id ?>"  <?php echo ($statu->status_id == $activity->activities_status_id ? 'selected' : '') ?>><?php echo $statu->status_description ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>   
                        <div class="col-md-3">
                            <label>Encerramento da atividade</label>
                            <input type="datetime-local" class="form-control" name="activities_end" value="<?php echo $activity->activities_end;?>">
                            <?php echo form_error('activities_end','<small class="form-text text-danger">','</small>')?>
                        </div>                        
                    </div>                                                            
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="activities_description" rows="12"><?php echo $activity->activities_description;?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-sm ml-3"><i class="far fa-save"></i>&nbsp;&nbsp;Salvar</button>                       
                        <a title="Voltar"  href="<?php echo base_url('activities/showDescription/'.$activity->activities_id);?>"
                            class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>                 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>