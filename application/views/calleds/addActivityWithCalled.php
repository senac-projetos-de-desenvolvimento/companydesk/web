<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>

    <div class="container-fluid">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('activities'); ?>">Atividades</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_add_activity">
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Cliente</label>
                                <input type="text" class="form-control" id="calleds_customers_end_id_ac"
                                value="<?php echo set_value('activities_customers_end_id');?>">
                                <?php echo form_error('activities_customers_end_id','<small class="form-text text-danger">','</small>')?>
                                <input type="hidden" name="calleds_customers_end_id" id="calleds_customers_end_id">
                        </div>
                        <div class="col-md-3">
                            <label>Parceiro</label>
                                <input type="text" class="form-control" id="calleds_customers_partner_id_ac"
                                    value="<?php echo set_value('activities_customers_partner_id');?>">
                                <?php echo form_error('activities_customers_partner_id','<small class="form-text text-danger">','</small>')?>
                                <input type="hidden" name="calleds_customers_partner_id" id="calleds_customers_partner_id">    
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="activities_users_id">
                                <?php foreach($users as $user): ?>
                                    <option value="<?php echo $user->id ?>"><?php echo $user->first_name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Origem</label>
                            <select class="custom-select" name="activities_origins_id">
                                <?php foreach($origins as $origin): ?>
                                    <option value="<?php echo $origin->origins_id ?>"><?php echo $origin->origins_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>                        
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Chamado</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="activities_calleds_id"
                                    value="<?php echo $called->calleds_id;?>" readonly>
                                <?php echo form_error('activities_calleds_id','<small class="form-text text-danger">','</small>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Prioridade</label>
                            <select class="custom-select" name="activities_priorities_id">
                                <?php foreach($priorities as $priority): ?>
                                    <option value="<?php echo $priority->priorities_id ?>"> <?php echo $priority->priorities_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Automação</label>
                            <select class="custom-select" name="activities_automations_id">
                                <?php foreach($automations as $automation): ?>
                                    <option value="<?php echo $automation->automations_id ?>"><?php echo $automation->automations_model ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Identificador</label>
                            <select class="custom-select" name="activities_types_identifier_id">
                                <?php foreach($types_identifier as $type_identifier): ?>
                                    <option value="<?php echo $type_identifier->types_identifier_id ?>"><?php echo $type_identifier->types_identifier_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Anexos</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="activities_attachments"
                                    value="<?php echo set_value('activities_attachments');?>">
                                <?php echo form_error('activities_attachments','<small class="form-text text-danger">','</small>')?>
                                <div class="input-group-btn">
                                    <a title="search" href="<?php echo base_url('customers/show_all_customers_calleds');?>"class="btn btn-outline-primary"><i class="fas fa-paperclip"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Início</label>
                            <input type="datetime-local" class="form-control" name="activities_start" 
                                value="<?php echo set_value('activities_start');?>">
                            <!-- value é o que vem lá do banco e o user é o que vem do controller-->
                            <?php echo form_error('activities_start','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Final</label>
                            <input type="datetime-local" class="form-control" name="activities_end"
                                value="<?php echo set_value('activities_end');?>">
                            <!-- value é o que vem lá do banco e o user é o que vem do controller-->
                            <?php echo form_error('activities_end','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>                   
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="activities_description"
                                rows="12"><?php echo set_value('activities_description');?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-save"></i>&nbsp;&nbsp; Salvar</button>
                        <a title="Voltar" href="<?php echo base_url('activities');?>"
                            class=" btn btn-success btn-sm float-right ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>