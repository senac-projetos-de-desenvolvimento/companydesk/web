<?php $this->load->view('layout/sidebar'); ?>



<!-- Main Content -->
<div id="content">

    <?php $this->load->view('layout/navbar');?>

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>

        <?php if($message = $this->session->flashdata('success')):?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;<?php echo $message;?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php endif;?>
        <?php if($message = $this->session->flashdata('error')):?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;<?php echo $message;?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php endif;?>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
                <a title="Cadastrar novo atendimento" href="<?php echo base_url('attendances/add')?>"
                    class=" btn btn-success btn-sm float-right"><i class="fas fa-user-plus"></i>&nbsp; Novo</a>
            </div> -->
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Origem</th>
                                <th class="text-center">Atividade</th>  
                                <th class="text-center">Cliente</th>                               
                                <th class="text-center">Parceiro</th>                                  
                                <th class="text-center">Usuário</th>
                                <th class="text-center">Inicio</th>
                                <th class="text-center">Final</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($attendances as $attendance):?>
                            <tr>
                                <td class="text-center" column style="width:100px"> <a href="<?php echo base_url('attendances/showDescription/'.$attendance->attendances_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a><?php echo ' '.$attendance->attendances_id ?></td>
                                <td class="text-center"> <?php echo $attendance->attendances_origins ?></td>
                                <td class="text-center"> <a href="<?php echo base_url('attendances/showActivity/'.$attendance->attendances_activities_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a><?php echo ' '.$attendance->attendances_activities_id ?></td>
                                <td class="text-center"> <a href="<?php echo base_url('activities/showCustomer/'.$attendance->attendances_customers_end_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a><?php echo ' '.$attendance->attendances_customers_end ?></td>
                                <td class="text-center"> <a href="<?php echo base_url('activities/showCustomer/'.$attendance->attendances_customers_partner_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a><?php echo ' '.$attendance->attendances_customers_partner ?></td> 
                                <td class="text-center"> <?php echo $attendance->attendances_user ?></td>
                                <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($attendance->attendances_start)) ?></td>
                                <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($attendance->attendances_end)) ?></td>
                            </tr>                           
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>