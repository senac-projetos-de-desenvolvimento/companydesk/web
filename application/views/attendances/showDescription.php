<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('attendances'); ?>">Atendimentos</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_edit_attendance">                                       
                    <?php foreach($customers as $customer): ?>
                        <?php 
                            if($attendance->attendances_customers_end_id == $customer->customers_id){
                                $attendance_customer_end = $customer->customers_social_reason;
                            }    
                            if($attendance->attendances_customers_partner_id == $customer->customers_id){
                                $attendance_customer_partner = $customer->customers_social_reason;
                            }                  
                        ?>
                    <?php endforeach; ?>                                               
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Cliente</label>
                            <div class="input-group"> 
                                <input type="text" class="form-control" name="attendances_customers_end_id"
                                     value="<?php echo $attendance_customer_end ?>" readonly>
                                <?php echo form_error('attendances_customers_end_id','<small class="form-text text-danger">','</small>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Parceiro</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="attendances_customers_partner_id"
                                    value="<?php echo $attendance_customer_partner;?>" readonly>
                                <?php echo form_error('attendances_customers_partner_id','<small class="form-text text-danger">','</small>')?>                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Atividade</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="attendances_activities_id" placeholder="Atividade"
                                    value="<?php echo $attendance->attendances_activities_id;?>" readonly>
                                <?php echo form_error('attendances_activities_id','<small class="form-text text-danger">','</small>')?>                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="attendances_users_id" disabled>
                                <?php foreach($users as $user):?>
                                    <option value="<?php echo $user->id ?>"  <?php echo ($user->id == $attendance->attendances_users_id ? 'selected' : '') ?>><?php echo $user->first_name ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Origem</label>
                            <select class="custom-select" name="attendances_origins_id" disabled>
                                <?php foreach($origins as $origin):?>
                                    <option value="<?php echo $origin->origins_id ?>"  <?php echo ($origin->origins_id == $attendance->attendances_origins_id ? 'selected' : '') ?>><?php echo $origin->origins_description ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="attendances_description" rows="12" readonly><?php echo $attendance->attendances_description;?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <a title="Voltar"  href="<?php echo base_url('activities/showDescription/'.$attendance->attendances_activities_id);?>"
                            class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a> 
                    </div>            
                </form>
            </div>
        </div>
    </div>
</div>