<?php $this->load->view('layout/sidebar'); ?>
<div id="content">

    <?php $this->load->view('layout/navbar');?>

    <div class="container-fluid">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form method="POST" name="form_edit">                

                <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-user-tie"></i>&nbsp; Dados empresariais</legend>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Razão Social</label>
                            <input type="text" class="form-control" name="customers_social_reason" placeholder="Razão Social"
                                value="<?php echo $customer->customers_social_reason;?>">
                            <?php echo form_error('customers_social_reason','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Nome Fantasia</label>
                            <input type="text" class="form-control" name="customers_fantasy_name"
                                placeholder="Nome Fantasia" value="<?php echo $customer->customers_fantasy_name;?>">
                            <?php echo form_error('customers_fantasy_name','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>CNPJ</label>
                            <input type="text" class="form-control cnpj" name="customers_cnpj" placeholder="CNPJ"
                                value="<?php echo $customer->customers_cnpj;?>">
                            <?php echo form_error('customers_cnpj','<small class="form-text text-danger">','</small>')?>
                        </div>
                          <div class="col-md-3">
                            <label>Inscrição estadual</label>
                            <input type="text" class="form-control" name="customers_state_registration" placeholder="Inscrição estadual"
                                value="<?php echo $customer->customers_state_registration;?>">
                            <?php echo form_error('customers_state_registration','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-4">
                            <label>Telefone fixo</label>
                            <input type="text" class="form-control phone_with_ddd" name="customers_telephone_fix" placeholder="Telefone fixo"
                                value="<?php echo $customer->customers_telephone_fix;?>">
                            <?php echo form_error('customers_telephone_fix','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Telefone móvel</label>
                            <input type="text" class="form-control cel_phone_with_ddd" name="customers_telephone"
                                placeholder="Telefone móvel" value="<?php echo $customer->customers_telephone;?>">
                            <?php echo form_error('customers_telephone','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="customers_email" placeholder="E-mail"
                                value="<?php echo $customer->customers_email;?>">
                            <?php echo form_error('customers_email','<small class="form-text text-danger">','</small>')?>
                        </div>                          
                    </div>
                </fieldset>
                <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-map-marker-alt"></i>&nbsp; Dados de endereço</legend>
                    <div class="form-group row mt-3">
                        <div class="col-md-6">
                            <label>Endereço</label>
                            <input type="text" class="form-control" name="customers_address" placeholder="Endereço"
                                value="<?php echo $customer->customers_address;?>">
                            <?php echo form_error('customers_address','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-2">
                            <label>Número</label>
                            <input type="text" class="form-control" name="customers_address_number"
                                placeholder="Número" value="<?php echo $customer->customers_address_number;?>">
                            <?php echo form_error('customers_address_number','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Complemento</label>
                            <input type="text" class="form-control" name="customers_address_complement" placeholder="Complemento"
                                value="<?php echo $customer->customers_address_complement;?>">
                            <?php echo form_error('customers_address_complement','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-3">
                            <label>Cidade</label>
                            <input type="text" class="form-control" name="customers_city" placeholder="Cidade"
                                value="<?php echo $customer->customers_city;?>">
                            <?php echo form_error('customers_city','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>CEP</label>
                            <input type="text" class="form-control cep" name="customers_cep" placeholder="CEP"
                                value="<?php echo $customer->customers_cep;?>">
                            <?php echo form_error('customers_cep','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Estado</label>
                            <input type="text" class="form-control fu" name="customers_federative_unit"
                                placeholder="Estado" value="<?php echo $customer->customers_federative_unit;?>">
                            <?php echo form_error('customers_federative_unit','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>País</label>
                            <input type="text" class="form-control" name="customers_country" placeholder="País"
                                value="<?php echo $customer->customers_country;?>">
                            <?php echo form_error('customers_country','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-info"></i>&nbsp; Informações</legend>
                    <div class="form-group row mt-3">
                          <div class="col-md-4">
                            <label>Ativo</label>
                            <select class = "form-control form-control-user" name="customers_defaulting">
                                <option value = "0" <?php echo ($customer->customers_defaulting == 0 ? 'selected' : ''); ?>>Não</option>
                                <option value = "1" <?php echo ($customer->customers_defaulting == 1 ? 'selected' : ''); ?>>Sim</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Número de bombas</label>
                            <input type="text" class="form-control" name="customers_number_pumps" placeholder="Número de bombas"
                                value="<?php echo $customer->customers_number_pumps;?>">
                            <?php echo form_error('customers_number_pumps','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Tipo de automação</label>
                            <input type="text" class="form-control" name="customers_automations_id" placeholder="Tipo de automação"
                                value="<?php echo $customer->customers_automations_id;?>">
                            <?php echo form_error('customers_automations_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-4">
                            <label>Tipo de identificador</label>
                            <input type="text" class="form-control" name="customers_types_identifier_id" placeholder="Tipo de identificador"
                                value="<?php echo $customer->customers_types_identifier_id;?>">
                            <?php echo form_error('customers_types_identifier_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Marcas das bombas</label>
                            <input type="text" class="form-control" name="customers_pumps_brand_id" placeholder="Marcas das bombas"
                                value="<?php echo $customer->customers_pumps_brand_id;?>">
                            <?php echo form_error('customers_pumps_brand_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Tipo de cliente</label>
                            <input type="text" class="form-control" name="customers_types_company_id" placeholder="Tipo de cliente"
                                value="<?php echo $customer->customers_types_company_id;?>">
                            <?php echo form_error('customers_types_company_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>  
                </fieldset>    
                <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-comments"></i>&nbsp; Observações</legend>
                    <div class="form-gropu row mt-3 mb-3">
                        <div class="col-md-12">
                            <label>Comentários</label>
                            <textarea class="form-control" name="customers_comments" rows="10"
                                placeholder="Comentários"><?php echo $customer->customers_comments;?></textarea>  
                                <?php echo form_error('customers_comments','<small class="form-text text-danger">','</small>')?>                                                      
                        </div>                        
                    </div> 
                </fieldset>             
                    <div class="form-group row">
                        <input type="hidden" class="form-control" name="customer_id" value="<?php echo $customer->customers_id; ?>">
                    </div>                 
                    <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-save"></i>&nbsp;&nbsp;Salvar</button>                                              
                    <a title="Voltar" href="<?php echo base_url('customers');?>"
                        class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>                        
            </div>
        </form>
    </div>
</div>