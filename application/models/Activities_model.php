<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Activities_model extends CI_Model{
    public function get_all(){
        $this->db->select([
            'activities.*',
            'customers.customers_id',
            'customers.customers_fantasy_name as activities_customers_end',

            'cus2.customers_id',
            'cus2.customers_fantasy_name as activities_customers_partner',

            'status.status_id',
            'status.status_description as activities_status',

            'users.id',
            'users.first_name as activities_user',

            'origins.origins_id',
            'origins.origins_description as activities_origins',

            'calleds.calleds_id',
            'calleds.calleds_id as activities_calleds',

            'priorities.priorities_id',
            'priorities.priorities_description as activities_priorities',           

            'automations.automations_id',
            'automations.automations_model as activities_automations',

            'types_identifier.types_identifier_id',
            'types_identifier.types_identifier_description as activities_types_identifier',

        ]);
        $this->db->join('customers','customers.customers_id = activities_customers_end_id', 'LEFT');
        $this->db->join('customers cus2','cus2.customers_id = activities_customers_partner_id', 'LEFT');
        $this->db->join('status','status.status_id = activities_status_id', 'LEFT');
        $this->db->join('users','users.id = activities_users_id', 'LEFT');
        $this->db->join('origins','origins.origins_id = activities_origins_id', 'LEFT');
        $this->db->join('calleds','calleds.calleds_id = activities_calleds_id', 'LEFT');
        $this->db->join('priorities','priorities.priorities_id = activities_priorities_id', 'LEFT');
        $this->db->join('automations','automations.automations_id = activities_automations_id', 'LEFT');
        $this->db->join('types_identifier','types_identifier.types_identifier_id = activities_types_identifier_id', 'LEFT');

        return $this->db->get('activities')->result();
    }

    public function get_custtomer_partner(){
         $this->db->select([
            'activities.*',
            'customers.customers_id',
            'customers.customers_fantasy_name as customers_partner',
         ]);
        $this->db->join('customers','customers.customers_id = activities_customers_partner_id', 'LEFT');
        return $this->db->get('activities')->result();
    }

    public function getAllAttendancesToActivity($activity_id){
        $sql = $this->db->query('
            select * from attendances where attendances_activities_id  = '.$activity_id
        );
        return $sql->result();
    }
    
    public function getCustomersEndAutoComplete($customer_id){
        $sql = $this->db->query("
            select * from customers where concat(customers_social_reason, customers_fantasy_name, customers_cnpj) like '%".$customer_id."%'
        ");
        return $sql->result_array();
    }


    public function getAllActivitiesToCalled($called_id){
        $this->db->select([
            'activities.*',
            'customers.customers_id',
            'customers.customers_fantasy_name as activities_customers_end',

            'cus2.customers_id',
            'cus2.customers_fantasy_name as activities_customers_partner',

            'status.status_id',
            'status.status_description as activities_status',

            'users.id',
            'users.first_name as activities_user',

            'origins.origins_id',
            'origins.origins_description as activities_origins',

            'calleds.calleds_id',
            'calleds.calleds_id as activities_calleds',

            'priorities.priorities_id',
            'priorities.priorities_description as activities_priorities',           

            'automations.automations_id',
            'automations.automations_model as activities_automations',

            'types_identifier.types_identifier_id',
            'types_identifier.types_identifier_description as activities_types_identifier',

        ]);
        $this->db->join('customers','customers.customers_id = activities_customers_end_id', 'LEFT');
        $this->db->join('customers cus2','cus2.customers_id = activities_customers_partner_id', 'LEFT');
        $this->db->join('status','status.status_id = activities_status_id', 'LEFT');
        $this->db->join('users','users.id = activities_users_id', 'LEFT');
        $this->db->join('origins','origins.origins_id = activities_origins_id', 'LEFT');
        $this->db->join('calleds','calleds.calleds_id = activities_calleds_id', 'LEFT');
        $this->db->join('priorities','priorities.priorities_id = activities_priorities_id', 'LEFT');
        $this->db->join('automations','automations.automations_id = activities_automations_id', 'LEFT');
        $this->db->join('types_identifier','types_identifier.types_identifier_id = activities_types_identifier_id', 'LEFT');
        $this->db->where('activities_calleds_id = ',$called_id);

        return $this->db->get('activities')->result();
    }
}

/*
            [activities_atachments] => 
            [activities_customers_partner_id] => 1
            [activities_customers_end_id] => 2
            [activities_users_id] => 1
            [activities_origins_id] => 1
            [activities_calleds_id] => 1
            [activities_priorities_id] => 2
            [activities_automations_id] => 1
            [activities_types_identifier_id] => 1
*/