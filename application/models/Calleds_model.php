<?php
defined('BASEPATH') OR exit('Ação não permitida.');
class Calleds_model extends CI_Model{
    public function get_all(){    
        $this->db->select([
            'calleds.*',
            'status.status_id',
            'status.status_description as calleds_status',

            'users.id',
            'users.first_name as calleds_user',

            // 'call_types.call_types_id',
            // 'call_types.call_types_description as calleds_call_types',

            'priorities.priorities_id',
            'priorities.priorities_description as calleds_priorities',

            'customers.customers_id',
            'customers.customers_fantasy_name as calleds_customers_end',

            'cus2.customers_id',
            'cus2.customers_fantasy_name as customers_partner',

            'automations.automations_id',
            'automations.automations_model as calleds_automations',

            'types_identifier.types_identifier_id',
            'types_identifier.types_identifier_description as calleds_types_identifier',

        ]);

        $this->db->join('status','status.status_id = calleds_status_id', 'LEFT');
        $this->db->join('users','users.id = calleds_users_id', 'LEFT');
        // $this->db->join('call_types','call_types.call_types_id = calleds_call_types_id', 'LEFT');
        $this->db->join('priorities','priorities.priorities_id = calleds_priorities_id', 'LEFT');
        $this->db->join('customers','customers.customers_id = calleds_customers_end_id', 'LEFT');
        $this->db->join('customers cus2','cus2.customers_id = calleds_customers_partner_id', 'LEFT');
        $this->db->join('automations','automations.automations_id = calleds_automations_id', 'LEFT');
        $this->db->join('types_identifier','types_identifier.types_identifier_id = calleds_types_identifier_id', 'LEFT');

        return $this->db->get('calleds')->result();
    }

    public function getAllActivitiesToCalled($called_id){
        $sql = $this->db->query('
            select * from activities where activities_calleds_id = '.$called_id
        );
        return $sql->result();
    }

    public function getCustomersEndAutoComplete($customer_id){
        $sql = $this->db->query("
            select * from customers where concat(customers_social_reason, customers_fantasy_name, customers_cnpj) like '%".$customer_id."%' and customers.customers_id not in(select calleds_customers_end_id from calleds where calleds_status_id != 2)
        ");
        return $sql->result_array();
    }
    public function getCustomersPartnerAutoComplete($customer_id){
        $sql = $this->db->query("
            select * from customers where concat(customers_social_reason, customers_fantasy_name, customers_cnpj) like '%".$customer_id."%'
        ");
        return $sql->result_array();
    }
}