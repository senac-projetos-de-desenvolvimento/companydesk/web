<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Attendances_model extends CI_Model{
    public function get_all(){
        $this->db->select([
            'attendances.*',

            'cus2.customers_id',
            'cus2.customers_fantasy_name as attendances_customers_partner',

            'customers.customers_id',
            'customers.customers_fantasy_name as attendances_customers_end',

            'origins.origins_id',
            'origins.origins_description as attendances_origins',
                
            'users.id',
            'users.first_name as attendances_user',
        ]);
        $this->db->join('customers','customers.customers_id = attendances_customers_end_id', 'LEFT');
        $this->db->join('customers cus2','cus2.customers_id = attendances_customers_partner_id', 'LEFT');
        $this->db->join('origins','origins.origins_id = attendances_origins_id', 'LEFT');
        $this->db->join('users','users.id = attendances_users_id', 'LEFT');

        return $this->db->get('attendances')->result();
    }

    public function getCustomersEndAutoComplete($customer_id){
        $sql = $this->db->query("
            select * from customers where concat(customers_social_reason, customers_fantasy_name, customers_cnpj) like '%".$customer_id."%'
        ");
        return $sql->result_array();
    }

    public function getAllAtendancesToActivity($activity_id){
        $this->db->select([
            'attendances.*',

            'cus2.customers_id',
            'cus2.customers_fantasy_name as attendances_customers_partner',

            'customers.customers_id',
            'customers.customers_fantasy_name as attendances_customers_end',

            'origins.origins_id',
            'origins.origins_description as attendances_origins',
                
            'users.id',
            'users.first_name as attendances_user',
        ]);
        $this->db->join('customers','customers.customers_id = attendances_customers_end_id', 'LEFT');
        $this->db->join('customers cus2','cus2.customers_id = attendances_customers_partner_id', 'LEFT');
        $this->db->join('origins','origins.origins_id = attendances_origins_id', 'LEFT');
        $this->db->join('users','users.id = attendances_users_id', 'LEFT');
        $this->db->where('attendances_activities_id= ',$activity_id);

        return $this->db->get('attendances')->result();
    }
}